-- general
lvim.format_on_save = false
lvim.lint_on_save = true

-- keymappings
lvim.leader = "space"

-- Load the plugins before calling their config
require("user.plugins").config() -- Additional plugins
require("user.builtin").config() -- Additional plugins
require("user.style").config() -- Additional plugins
require("user.keymaps").config() -- Additional keymappings

lvim.colorscheme = "kanagawa"
require("user.dap").config() -- untested but seems cool

-- Default options
vim.opt.timeoutlen = 200
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
vim.opt.foldlevel = 10
vim.opt.relativenumber = true


lvim.builtin.treesitter.ignore_install = {"phpdoc"}

-- Language Specific
-- =========================================
vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, {
	"rust_analyzer",
	"scala",
	"metals",
})



-- ## ONe liners which prove useful from time to time.
-- Formtting a xml file:
-- :%!python3 -c "import xml.dom.minidom, sys; print(xml.dom.minidom.parse(sys.stdin).toprettyxml())"
-- Updating all packages in the package.json file:
-- npm outdated | awk 'NR>1 {print $1"@"$4}' | xargs npm install
