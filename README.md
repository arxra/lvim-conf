# lvim-conf

My lunar-vim extensions repository.
Most of it is copy+pejst from [abzcoding](https://github.com/abzcoding/lvim)'s LunarVim configuration.

![Showcasing rust](./img/Screenshot_20220316_104159.png)
