local M = {}

M.config = function()
	lvim.builtin.which_key.mappings["t"] = {
		name = "Trouble/Test",
		d = { "<cmd>Trouble lsp_document_diagnostics<cr>", "Diagnosticss" },
		F = { "<cmd>Trouble lsp_definitions<cr>", "Definitions" },
		l = { "<cmd>Trouble loclist<cr>", "LocationList" },
		q = { "<cmd>Trouble quickfix<cr>", "QuickFix" },
		r = { "<cmd>Trouble lsp_references<cr>", "References" },
		T = { "<cmd>Trouble todo<cr>", "TODO tags" },
		w = { "<cmd>Trouble lsp_workspace_diagnostics<cr>", "Diagnosticss" },
		t = { "<cmd>TestNearest -- --nocapture<cr>", "TestNearest" },
		f = { "<cmd>TestFile -- --nocapture<cr>", "TestFile" },
		p = { "<cmd>TestSuite -- --nocapture<cr>", "TestSuite" },
	}

  lvim.builtin.which_key.mappings.l.c = { "<cmd>LspSettings buffer<cr>", "Configure current LSP" }
	lvim.builtin.which_key.mappings["P"] = { "<cmd>Telescope projects<CR>", "Projects" }
  lvim.keys.insert_mode["jk"] = "<ESC>:w<CR>"
  lvim.keys.insert_mode["<C-s>"] = "<cmd>lua vim.lsp.buf.signature_help()<cr>"

  if lvim.builtin.persistence then
    lvim.builtin.which_key.mappings["q"] = {
      name = " Quit",
      d = { "<cmd>lua require('persistence').stop()<cr> | :qa!<cr>", "Quit without saving session" },
      l = { "<cmd>lua require('persistence').load(last=true)<cr>", "Restore last session" },
      s = { "<cmd>lua require('persistence').load()<cr>", "Restore for current dir" },
    }
  end

end


return M
