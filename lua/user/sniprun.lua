local M = {}

M.config = function()
	local status_ok, sniprun = pcall(require, "sniprun")
	if not status_ok then
		return
	end

	sniprun.setup({
		display = { "NvimNotify" },
	})
end

return M
