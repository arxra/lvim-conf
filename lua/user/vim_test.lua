local M = {}

M.config = function()
  vim.g["test#strategy"] = "dispatch"
end

return M
