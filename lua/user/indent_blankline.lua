local M = {}

M.config = function()
	local status_ok, bl = pcall(require, "indent_blankline")
	if not status_ok then
		return
	end

	bl.setup({
		enabled = true,
		show_current_context = true,
		show_current_context_start = false,
		show_trailing_blankline_indent = false,
    show_first_indent_level = false,
    show_foldtext = false,
    max_indent_increase = 1,
    use_treesitter = true,
		bufname_exclude = { "README.md" },
		buftype_exclude = { "terminal", "nofile" },
		filetype_exclude = {
			"alpha",
			"log",
			"gitcommit",
			"dapui_scopes",
			"dapui_stacks",
			"dapui_watches",
			"dapui_breakpoints",
			"dapui_hover",
			"dbui",
			"UltestSummary",
			"UltestOutput",
			"vimwiki",
			"markdown",
			"git",
			"TelescopePrompt",
			"undotree",
			"flutterToolsOutline",
			"org",
			"help",
			"startify",
			"dashboard",
			"packer",
			"neogitstatus",
			"Outline",
			"Trouble",
			"lspinfo",
			"", -- for all buffers without a file type
		},
		context_patterns = {
			"class",
			"return",
			"function",
			"method",
			"^if",
			"^do",
			"^switch",
			"^while",
			"jsx_element",
			"^for",
			"^object",
			"^table",
			"block",
			"arguments",
			"if_statement",
			"else_clause",
			"jsx_element",
			"jsx_self_closing_element",
			"try_statement",
			"catch_clause",
			"import_statement",
			"operation_type",
		},
	})
end

return M
