local M = {}

M.config = function()
	local formatters = require("lvim.lsp.null-ls.formatters")
	formatters.setup({
		{
			exe = "isort",
			args = { "--profile", "black" },
			filetypes = { "python" },
		},
		{
			exe = "black",
			args = { "-S", "--line-length=120" },
			filetypes = { "python" },
		},
		{ exe = "prettierd" },
		{ exe = "stylua" },
		{ exe = "latexindent" },
		{ exe = "shfmt" },
		{ exe = "rustfmt" },
		-- { exe = "scalafmt" },
	})

	local linters = require("lvim.lsp.null-ls.linters")
	linters.setup({
		{ exe = "mypy" },
		{ exe = "vale", args = {"--config", "~/.vale.ini"} },
	})
end

return M
