local M = {}
-- Additional Plugins
M.config = function()
  lvim.plugins = {
    {
      "folke/trouble.nvim",
      requires = "kyazdani42/nvim-web-devicons",
      config = function()
        require("trouble").setup()
      end,
      cmd = "Trouble",
      -- event = "BufRead",
    },
    {
      "simrat39/symbols-outline.nvim",
      setup = function()
        require("user.symbols_outline").config()
      end,
      event = "BufReadPost",
    },
    { "anufrievroman/vim-angry-reviewer", ft = { "tex", "latex" } },
    {
      "kevinhwang91/nvim-bqf",
      config = function()
        require("user.bqf").config()
      end,
      event = "BufRead",
    },
    {
      "kdheepak/cmp-latex-symbols",
      requires = "hrsh7th/nvim-cmp",
      ft = "tex",
    },
    { "hrsh7th/cmp-cmdline" },

    --#region Language stuff
    {
      "simrat39/rust-tools.nvim",
      config = function()
        require("user.rust_tools").config()
      end,
      ft = { "rust", "rs" },
    },
    {
      "Saecki/crates.nvim",
      event = { "BufRead Cargo.toml" },
      requires = { { "nvim-lua/plenary.nvim" } },
      config = function()
        require("crates").setup()
      end,
    },
    {
      "vuki656/package-info.nvim",
      event = { "BufRead package.json" },
      config = function()
        require("package-info").setup()
      end,
    },
    {
      "scalameta/nvim-metals",
      config = function()
        local metals_config = require("metals").bare_config()
        metals_config.on_attach = function()
          require("lsp").common_on_attach()
        end
        metals_config.settings = {
          showImplicitArguments = false,
          showInferredType = true,
          excludedPackages = {},
        }
        metals_config.init_options.statusBarProvider = true
        require("metals").initialize_or_attach({ metals_config })
      end,
      ft = { "scala", "sbt" },
    },
    {
      "iamcco/markdown-preview.nvim",
      run = "cd app && npm install",
      ft = "markdown",
    },
    -- Database stuff
    {
      "rcarriga/nvim-dap-ui",
      config = function()
        local dap, dapui = require("dap"), require("dapui")
        dap.listeners.after.event_initialized["dapui_config"] = function()
          dapui.open()
        end
        dap.listeners.before.event_terminated["dapui_config"] = function()
          dapui.close()
        end
        dap.listeners.before.event_exited["dapui_config"] = function()
          dapui.close()
        end
        require("dapui").setup()
      end,
      ft = { "python", "rust", "go", "c", "cpp" },
      requires = { "mfussenegger/nvim-dap" },
    },
    {
      "kristijanhusak/vim-dadbod-completion",
    },
    {
      "kristijanhusak/vim-dadbod-ui",
      cmd = {
        "DBUIToggle",
        "DBUIAddConnection",
        "DBUI",
        "DBUIFindBuffer",
        "DBUIRenameBuffer",
      },
      requires = {
        {
          "tpope/vim-dadbod",
          opt = true,
        },
      },
      opt = true,
    },
    -- Run Things
    {
      "michaelb/sniprun",
      run = "bash ./install.sh",
      config = function()
        require("user.sniprun").config()
      end,
    },
    { "tpope/vim-dispatch" },
    {
      "vim-test/vim-test",
      cmd = { "TestNearest", "TestFile", "TestSuite", "TestLast", "TestVisit" },
      keys = { "<localleader>tf", "<localleader>tn", "<localleader>ts" },
      config = function()
        require("user.vim_test").config()
      end,
    },
    {
      "tpope/vim-surround",
      keys = { "c", "d", "y" },
    },
    { "vim-scripts/VisIncr" },
    {
      "nathom/filetype.nvim", -- Replace default filetype.vim which is slower
      config = function()
        require("filetype").setup({
          overrides = {
            literal = {
              ["kitty.conf"] = "kitty",
              [".gitignore"] = "conf",
              [".eslintrc"] = "json",
              [".stylelintrc"] = "json",
              [".htmlhintrc"] = "json",
              ["Jenkinsfile"] = "groovy",
              ["Vagrantfile"] = "ruby",
              ["log"] = "log",
            },
            extensions = {
              tf = "terraform",
              log = "log",
            },
          },
        })
      end,
    },
    { "editorconfig/editorconfig-vim" },
    -- Eye Candy
    { "EdenEast/nightfox.nvim" },
    {
      "rebelot/kanagawa.nvim",
      config = require("kanagawa").setup({
        undercurl = true, -- enable undercurls
        commentStyle = "italic",
        functionStyle = "NONE",
        keywordStyle = "italic",
        statementStyle = "bold",
        typeStyle = "NONE",
        variablebuiltinStyle = "italic",
        specialReturn = true, -- special highlight for the return keyword
        specialException = true, -- special highlight for exception handling keywords
        transparent = true, -- do not set background color
        dimInactive = false, -- dim inactive window `:h hl-NormalNC`
        globalStatus = true, -- adjust window separators highlight for laststatus=3
        colors = {},
        overrides = {},
      }),
    },
    {
      "folke/todo-comments.nvim",
      requires = "nvim-lua/plenary.nvim",
      config = function()
        require("user.todo_comments").config()
      end,
      event = "BufRead",
    },
    { "mtdl9/vim-log-highlighting", ft = { "text", "log" } },
    {
      "norcalli/nvim-colorizer.lua",
      config = function()
        require("user.colorizer").config()
      end,
      event = "BufRead",
    },
    { -- For matching on surrounding keywords, and showing the closure in python
      "andymass/vim-matchup",
      event = "BufReadPost",
      config = function()
        vim.g.matchup_enabled = 1
        vim.g.matchup_surround_enabled = 1
        vim.g.matchup_matchparen_deferred = 1
        vim.g.matchup_matchparen_offscreen = { method = "popup" }
      end,
    },
    {
      "ray-x/lsp_signature.nvim",
      config = function()
        require("user/lsp_signature").config()
      end,
      event = { "BufRead", "BufNew" },
    },
    {
      "lukas-reineke/indent-blankline.nvim",
      setup = function()
        vim.g.indent_blankline_char = "▏"
      end,
      config = function()
        require("user.indent_blankline").config()
      end,
      event = "BufRead",
    },
    {
      "j-hui/fidget.nvim",
      config = function()
        require("user.fidget_spinner").config()
      end,
    },
  }
end

return M
