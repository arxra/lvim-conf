local M = {}

M.config = function()
  local kind = require("user.lsp_kind")
  -- INFO: User Config for predefined plugins
  lvim.builtin.alpha.active = false
  lvim.builtin.autopairs.active = true
  lvim.builtin.dap.active = true
  lvim.builtin.lualine.active = true
  lvim.builtin.lualine.options.globalstatus = true
  lvim.builtin.notify.active = true
  lvim.builtin.nvimtree.setup.open_on_setup = true
  lvim.builtin.nvimtree.setup.renderer.indent_markers.enable = true
  lvim.builtin.nvimtree.side = "left"
  lvim.builtin.project.active = true
  lvim.builtin.telescope.defaults.preview = { treesitter = false } -- Helps with timeouts, ruins the highlight somewhat
  lvim.builtin.terminal.active = true

  --- LuaLine
  local components = require("lvim.core.lualine.components")
  function M.split(str, sep)
    local res = {}
    for w in str:gmatch("([^" .. sep .. "]*)") do
      if w ~= "" then
        table.insert(res, w)
      end
    end
    return res
  end

  local filePath = function()
    local fp = vim.fn.fnamemodify(vim.fn.expand("%"), ":~:.")
    local tbl = M.split(fp, "/")
    local len = #tbl
    local where = ""

    if len > 2 and tbl[1] ~= "~" then
      return "/" .. table.concat(tbl, "/", len - 1) .. where
    else
      return fp .. where
    end
  end

  lvim.builtin.lualine.sections.lualine_b = {
    components.branch,
    components.diff,
    { filePath },
    { "location", padding = { left = 0, right = 1 } },
    components.python_env,
  }

  lvim.builtin.lualine.sections.lualine_c = {}

  -- Only virtual popup of errors.
  -- https://github.com/LunarVim/LunarVim/issues/1712
  lvim.lsp.diagnostics.underline = true
  lvim.lsp.diagnostics.virtual_text = false
  -- vim.api.nvim_create_augroup("CursorHold", {
  -- 	pattern = {"*"},
  -- 	command = "lua vim.diagnostic.open_float(0,{scope='line'})",
  -- })

  -- =========================================
  lvim.builtin.cmp.sources = {
    { name = "nvim_lsp" },
    { name = "buffer", max_item_count = 5, keyword_length = 5 },
    { name = "path", max_item_count = 5 },
    { name = "luasnip", max_item_count = 3 },
    { name = "nvim_lua" },
    { name = "calc" },
    { name = "emoji" },
    { name = "treesitter" },
    { name = "crates" },
  }
  require("cmp").setup.cmdline(":", {
    sources = {
      { name = "cmdline" },
    },
  })
  lvim.builtin.cmp.experimental = {
    ghost_text = false,
    native_menu = false,
    custom_menu = true,
  }

  lvim.builtin.cmp.formatting.source_names = {
    buffer = "(Buffer)",
    nvim_lsp = "(LSP)",
    luasnip = "(Snip)",
    treesitter = "",
    nvim_lua = "(NvLua)",
    spell = "暈",
    emoji = "",
    path = "",
    calc = "",
    ["vim-dadbod-completion"] = "𝓐",
  }

  -- LSP
  -- =========================================
  lvim.lsp.buffer_mappings.normal_mode["K"] = {
    "<cmd>lua require('user.builtin').show_documentation()<CR>",
    "Show Documentation",
  }
  lvim.lsp.diagnostics.signs.values = {
    { name = "DiagnosticSignError", text = kind.icons.error },
    { name = "DiagnosticSignWarn", text = kind.icons.warn },
    { name = "DiagnosticSignInfo", text = kind.icons.info },
    { name = "DiagnosticSignHint", text = kind.icons.hint },
  }

  -- Telescope
  -- =========================================
  -- lvim.builtin.telescope.defaults.path_display = { shorten = 10 }
lvim.builtin.telescope.defaults.file_ignore_patterns = {
    "%.7z",
    "%.burp",
    "%.bz2",
    "%.cache",
    "%.class",
    "%.dll",
    "%.docx",
    "%.dylib",
    "%.epub",
    "%.exe",
    "%.flac",
    "%.ico",
    "%.ipynb",
    "%.jar",
    "%.jpeg",
    "%.jpg",
    "%.lock",
    "%.met",
    "%.mkv",
    "%.mp4",
    "%.otf",
    "%.pdb",
    "%.pdf",
    "%.png",
    "%.rar",
    "%.sqlite3",
    "%.svg",
    "%.tar",
    "%.tar.gz",
    "%.ttf",
    "%.webp",
    "%.zip",
    ".dart_tool/",
    ".git/",
    ".github/",
    ".gradle/",
    ".idea/",
    ".settings/",
    ".vale/",
    ".vscode/",
    "__pycache__/",
    "__pycache__/*",
    "build/",
    "env/",
    "gradle/",
    "node_modules/",
    "node_modules/*",
    "smalljre_*/*",
    "target/",
    "vendor/*",
  }

  lvim.builtin.telescope.defaults.mappings = {
    i = {
      ["<esc>"] = require("telescope.actions").close,
      ["<C-y>"] = require("telescope.actions").which_key,
    },
  }

  -- Treesitter
  -- =========================================
  lvim.builtin.treesitter.context_commentstring.enable = true
  -- lvim.builtin.treesitter.ensure_installed = "all"
  lvim.builtin.treesitter.highlight.disable = { "org" }
  lvim.builtin.treesitter.highlight.aditional_vim_regex_highlighting = { "org" }
  lvim.builtin.treesitter.ignore_install = { "haskell", "norg" }
  lvim.builtin.treesitter.incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = "<C-n>",
      node_incremental = "<C-n>",
      scope_incremental = "<C-s>",
      node_decremental = "<C-r>",
    },
  }
  lvim.builtin.treesitter.indent = { enable = true, disable = { "yaml", "python" } } -- treesitter is buggy :(
  lvim.builtin.treesitter.matchup.enable = true
  lvim.builtin.treesitter.query_linter = {
    enable = true,
    use_virtual_text = false,
    lint_events = { "BufWrite", "CursorHold" },
  }

  lvim.builtin.gitsigns.opts.current_line_blame = true
end

M.show_documentation = function()
  local filetype = vim.bo.filetype
  if vim.tbl_contains({ "vim", "help" }, filetype) then
    vim.cmd("h " .. vim.fn.expand("<cword>"))
  elseif vim.fn.expand("%:t") == "Cargo.toml" then
    require("crates").show_popup()
  elseif vim.tbl_contains({ "man" }, filetype) then
    vim.cmd("Man " .. vim.fn.expand("<cword>"))
  else
    vim.lsp.buf.hover()
  end
end

return M
